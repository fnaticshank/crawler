import sys
import urlparse

import requests
from bs4 import BeautifulSoup
from Queue import Queue

urls_seen = set()          # Used to avoid putting duplicates in queue
urls_remembered = set()    # For reporting to user
visited_links = set()       # Used to avoid re-processing a page
links_remembered = set()   # For reporting to user


def crawl(url, max_depth):
    root = url
    host = get_host_name(url)

    q = Queue()
    depth = 0
    q.put((root, depth))

    while not q.empty():
        current_url, depth = q.get()

        try:
            visited_links.add(current_url)
            page_links = fetch_all_links(current_url)

            for link in page_links:
                if link not in urls_seen:
                    q.put((link, depth+1))
                    urls_seen.add(link)


        except Exception, e:
            pass


def not_visited(url):
    """Returns url if the URL has not already been visited"""
    return (url not in visited_links)


def get_host_name(url):
    """Returns the netlock/ hostname of the url from urlparsed object"""
    return urlparse.urlparse(url)[1]


def make_clean_url(url):
    """Returns the base url and strips out querystring parameteres"""
    return urlparse.urldefrag(url)[0]


def fetch_all_links(url):
    """This function creates a request object and fetches the successive url"""

    url_list = []

    try:
        r = requests.get(url)
        if r.status_code == 200:

            print "Fetching url..."
            print r.status_code
            content = r.content
            soup = BeautifulSoup(content, "lxml")

            # scan for all anchor tags
            tags = soup('a')

            for a in tags:
                href = a.get("href")
                if href is not None:
                    new_url = urlparse.urljoin(url, href)
                    if new_url not in url_list:
                        url_list.append(make_clean_url(new_url))
            return url_list

        elif r.status_code == 403:
            print "Error: 403 Forbidden url"
        elif r.status_code == 404:
            print "Error: 404 URL not found"
        else:
            print "Make sure you have everything correct."

    except TypeError:
        print "Oops! Try again"


if __name__ == '__main__':
    url = sys.argv[1]

    page = fetch_all_links(url)
    for i, url in enumerate(page):
        print "%d. %s" % (i, url)
